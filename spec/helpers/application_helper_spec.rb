require 'spec_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "page_title" do
    it "page_titleが空文字の場合" do
      expect(full_title("")).to eq "BIGBAG Store"
    end

    it "page_titleがnilの場合" do
      expect(full_title(nil)).to eq "BIGBAG Store"
    end

    it "page_titleが空でない場合" do
      expect(full_title("productname")).to eq "productname - BIGBAG Store"
    end
  end
end
