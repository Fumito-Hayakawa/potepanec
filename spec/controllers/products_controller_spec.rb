require 'rails_helper'
require 'spree/testing_support/factories'

describe Potepan::ProductsController, type: :controller do
  describe "showコントローラーテスト" do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "正常にresponseする" do
      expect(response).to be_successful
    end

    it '@productの割り当て' do
      expect(assigns(:product)).to eq product
    end

    it 'show templateのrender' do
      expect(response).to render_template :show
    end
  end
end
