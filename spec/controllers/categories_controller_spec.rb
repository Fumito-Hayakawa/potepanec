require 'rails_helper'

describe Potepan::CategoriesController, type: :controller do
  let(:bag_taxon) { create(:taxon, name: 'bag') }
  let(:products) { create_list(:product, 5, taxons: [bag_taxon]) }

  describe "showコントローラーのテスト" do
    before do
      get :show, params: { id: bag_taxon.id }
    end

    it "正常にレスポンスする" do
      expect(response).to be_successful
    end

    it "@taxonの割り当て" do
      expect(assigns(:taxon)).to eq bag_taxon
    end

    it "@productsにbag_taxon.productsがあるか" do
      expect(assigns(:products)).to match_array(products)
    end
  end
end
