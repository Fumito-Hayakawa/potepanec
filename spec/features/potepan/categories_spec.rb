require 'rails_helper'

RSpec.feature "Categoriesページの表示", type: :feature do
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: other_taxonomy) }
  let(:other_taxonomy) { create(:taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, master: product_variant, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [taxon]) }
  let(:test_product) { create(:product, name: "test bag", taxons: [other_taxon]) }
  let(:product_image) { create(:image) }
  let(:product_variant) { create(:variant, is_master: true, images: [product_image]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "カテゴリと紐づいた商品名、価格を表示する" do
    expect(page).to have_link taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it "別の商品名を表示しない" do
    expect(page).not_to have_content test_product.name
  end

  it "商品画像が正しく表示できていること" do
    expect(page).to have_selector "img[alt$='#{product.name}image']"
  end

  it "別の商品詳細ページへリンクする" do
    within '.panel-body', match: :first do
      expect(page).to have_link other_taxon.name
      expect(page).to have_content other_taxonomy.name
    end
    expect(page).to have_content other_product.name
    expect(page).to have_content other_product.display_price
  end

  it "categoriesページからproductにリンクする" do
    expect(page).to have_link product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
