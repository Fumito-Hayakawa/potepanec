require 'rails_helper'

RSpec.feature "商品詳細ページを表示する", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細ページのタイトルを表示する" do
    expect(page).to have_content product.name
  end

  scenario "商品の価格を表示する" do
    expect(page).to have_content product.price
  end

  scenario "商品の説明文を表示する" do
    expect(page).to have_content product.description
  end

  scenario "一覧ページへ戻る" do
    expect(page).to have_link "一覧ページへ戻る"
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(product.taxons.ids.first)
  end
end
